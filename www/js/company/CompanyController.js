app.controller('CompanyController', ['$scope', '$http', '$ionicModal', function ($scope, $http, $ionicModal) {
  //$scope.menus = Menu.query();
  $scope.searchParams = {
    "keyword": null
  };
  $scope.companyList = [];
  $scope.searchCompany = function () {
    console.log($scope.searchParams.keyword);
    var params = {
      "keyword": $scope.searchParams.keyword
    };
    $http.get(host + '/companies', {params: params, cache: true})
      .then(function (response) {
        console.log(response);
        $scope.companyList = response.data.data;
      });
  };

  // Create the signup modal that we will use later
  $ionicModal.fromTemplateUrl('templates/company/modals/company-detail.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
  });


  // Open the stock price modal
  $scope.openCompanyDetail = function (company) {
    console.log(company);
    $scope.modal.show();
    $scope.company = company;
    $scope.returnCompany = null;
    var params = {
      "code": $scope.company.stockCode,
      "date": "2016-11-03"
    };
    $http.get(host + '/finances', {params: params, cache: true})
      .then(function (response) {
        console.log(response);
        $scope.returnCompany = response.data.data[0];
        $scope.returnCompany.stock.value = $scope.returnCompany.stock.price * $scope.returnCompany.stock.count
        console.log($scope.returnCompany);
      });
  };

  // Triggered in the stock price modal to close it
  $scope.closeCompanyDetail = function () {
    $scope.modal.hide();
  };

}]);
