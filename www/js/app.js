// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('mmApp', ['ionic', 'ui.router', 'ionic-datepicker']);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/login");

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'templates/auth/login.html',
      controller: 'LoginController'
    })
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/sidemenu.html',
      controller: 'AppCtrl'
    })
    .state('app.crawl', {
      url: '/crawl',
      views: {
        'menuContent': {
          templateUrl: 'templates/crawl/crawl.html',
          controller: 'CrawlController'
        }
      }
    })
    .state('app.company', {
      url: '/company',
      views: {
        'menuContent': {
          templateUrl: 'templates/company/company.html',
          controller: 'CompanyController'
        }
      }
    })
    .state('app.screen', {
      url: '/screen',
      views: {
        'menuContent': {
          templateUrl: 'templates/screen/screen.html',
          controller: 'ScreenController'
        }
      }
    })
    .state('app.portfolio', {
      url: '/portfolio',
      views: {
        'menuContent': {
          templateUrl: 'templates/portfolio/portfolio.html',
          controller: 'PortfolioController'
        }
      }
    })
    .state('app.trade', {
      url: '/trade',
      views: {
        'menuContent': {
          templateUrl: 'templates/trade/trade.html',
          controller: 'TradeController'
        }
      }
    })
    .state('app.userinfos', {
      url: '/user-infos',
      views: {
        'menuContent': {
          templateUrl: 'templates/auth/user-info.html',
          controller: 'UserController'
        }
      }
    });
}]);

app.run(function ($ionicPlatform, $http, storage, $rootScope, $state) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  $http.defaults.headers.common.Authorization = storage.get('token');
  $rootScope.token = storage.get('token');
  $rootScope.user = storage.get('userData');
});
