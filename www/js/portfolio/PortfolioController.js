app.controller('PortfolioController', function ($scope, $http, $interval, $ionicLoading) {
    $scope.show = function() {
        $ionicLoading.show({
          template: 'Loading...'
        }).then(function(){
        });
      };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
    });
  };


    $scope.getData = function() {
            $http.get(cybos_api + "/portfolio/")
            .then(function (response) {
                $scope.account_info = response.data
                $scope.stock_list = response.data.portfolios;
                $scope.hide();
            });
    };

    if ($scope.stock_list) {
        $interval($scope.getData, 50000);
    }
    $scope.show();
    $scope.getData();
});


