app.factory('cybosOrderFactory', ["$http", function($http) {
    var info_url = cybos_api + "/stock/info";
    var buy_url = cybos_api + "/stock/buy";
    var service = {};

    var _findByStockCode = function(list, stockcode) {
        var find_by_stock_code = function(obj) {
            return obj["종목코드"] == stockcode;
        }
        return list.find(find_by_stock_code);
    };

    service.makeOrder = function(stock_list) {
        var prep_data = stock_list.map(function(stock) {
            return {
                "stock_code": stock.stock_code,
                "order_type": stock.order_type.id * 1,
                "order_price": stock.order_price * 1,
                "amount": stock.amount * 1
            };
        });
        $http.post(buy_url, prep_data)
             .success(function(data, status, headers, config) {
                console.log(data);
             })
             .error(function(data, status, headers, config) {
                console.error(status, data);
             });
        console.log("주문: ", prep_data);
    };

    service.getCurrentInfo = function(stock_list) {
        $http.post(info_url, stock_list)
        .success(function(data, status, headers, config){
            if (data.length == stock_list.length) {
                // data = [stock1, stock2]
                // each stock has the following features
                //5일회전률 60.16999816894531
                //52주최고가 903000
                //52주최저가 542000
                //EPS 21869
                //PER 38.459999084472656
                //거래량 62848
                //시간 1515
                //장구분 50
                //전일거래량 85601
                //전일종가 829000
                //종목명 "NAVER"
                //종목코드 "A035420"
                //체결강도 92.51000213623047
                //체결구분 49
                //총상장주식수 32962679
                //현재가 841000
                return stock_list.map(function(stock) {
                    var cybos_data = _findByStockCode(data, stock.stock_code);
                    stock.price = cybos_data["현재가"];
                    stock.PER = cybos_data['PER'];
                    stock.EPS = cybos_data['EPS'];
                    stock.stock_name = cybos_data['종목명'];
                });
            } else {
                console.log("ERR inside cybosPriceUpdate Factory");
            }
        })
        .error(function(data, status, headers, config){
            console.log(status);
        })
    };

    return service;
}]);


app.factory('stockFactory', function() {
    var _stockList = [];
    var service = {}

    service.has = function(stock_code) {
        var stock_code_list = _stockList.map(function(i) { return i.stock_code });
        var idx = stock_code_list.indexOf(stock_code);
        return idx != -1;
    };

    service.add = function(stock_code) {
        if (!service.has(stock_code)) {
            _stockList.push({stock_code: stock_code});
        }
        return _stockList;
    };

    service.remove = function(item) {
        var stock_code_list = _stockList.map(function(i) { return i.stock_code });
        var idx = stock_code_list.indexOf(item.stock_code);
        if (idx != -1) {
            _stockList.splice(idx, 1);
        }
        return _stockList;
    };

    service.getter = function() {
        return _stockList;
    };

    service.update = function(stockList) {
        _stockList = stockList;
        return _stockList;
    };

    service.validate = function() {
        for(var i=0; i < _stockList.length; i++) {
            var obj = _stockList[i];
            if (obj.order_price < 0 || obj.amount < 0 || typeof obj.order_price == "undefined" || !obj.amount) {
                console.warn("Order Validation Failed");
                return false;
            }
        }
        return true;
    };

    return service;
});

