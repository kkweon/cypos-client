app.controller('TradeController', function ($scope, $http, $interval, stockFactory, cybosOrderFactory) {
    $scope.stockList = [];
    $scope.item = {
        stock_code: ""
    };

    $scope.getInfo = function() {
        var temp = $scope.stockList.map(function(stock) {
            return stock.stock_code;
        });
    }

    $scope.addStock = function() {
        if ($scope.item.stock_code) {
            $scope.stockList = stockFactory.add($scope.item.stock_code);
            $scope.item.stock_code = "";
            $scope.update();
        }
    };

    $scope.removeStock = function(stock) {
        $scope.stockList = stockFactory.remove(stock);
    }

    $scope.makeOrder = function() {
        if (stockFactory.getter().length > 0) {
            stockFactory.update($scope.stockList);
            if (stockFactory.validate()) {
                cybosOrderFactory.makeOrder(stockFactory.getter());
            }
        }
    };

    $scope.reset = function() {
        $scope.stockList = stockFactory.update([]);
    }

    $scope.update = function() {
        cybosOrderFactory.getCurrentInfo($scope.stockList);
    };

    $scope.order_types = [
        {id: 1, label:"매도"},
        {id: 2, label:"매수"}
    ];

});


