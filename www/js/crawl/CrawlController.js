app.controller('CrawlController', ['$scope', '$http', '$filter','$ionicPopup', '$ionicModal', function ($scope, $http, $filter, $ionicPopup, $ionicModal) {
  //주가 정보 가져오기
  $scope.stockList = [];
  $scope.financeList = [];
  $scope.currentTime = new Date().getTime();
  $scope.date = new Date(Date.now());

  $scope.datepickerObject = {
    titleLabel: '날짜 선택',  //Optional
    todayLabel: '오늘',  //Optional
    closeLabel: '닫기',  //Optional
    setLabel: '선택',  //Optional
    setButtonType: 'button-light',  //Optional
    closeButtonType: 'button-positive',  //Optional
    mondayFirst: false,  //Optional
    templateType: 'popup', //Optional
    showTodayButton: false, //Optional
    modalHeaderColor: 'bar-positive', //Optional
    modalFooterColor: 'bar-positive', //Optional
    callback: function (val) {  //Mandatory
      if (!val) return;
      $scope.date = $filter('date')(val, 'yyyy-MM-dd');
    },
    dateFormat: 'yyyy-MM-dd', //Optional
    closeOnSelect: true, //Optional
  };


  $scope.getStockLists = function () {
    var date = $filter('date')($scope.date, 'yyyy-MM-dd');
    var params = {
      date: date
    };
    $http.get(host + '/stocks', {params: params, cache: true})
      .then(function (response) {
        console.log(response);
        $scope.stockList = response.data.data;
        console.log($scope.stockList);
      });
  };
  $scope.getStockLists();

  $scope.dailyStocks = function () {
    $http.post(host + '/daily-stock')
      .then(function (response) {
        console.log(response);
        $scope.stockList = response.data.data;
        console.log($scope.stockList);
      });
  };

  $scope.crawlDart = function () {
    $http.post(host + '/dart-daily')
      .then(function (response) {
        console.log(response);
        $scope.dailyDart()
      });
  };

  $scope.dailyDart = function () {
    $http.get(host + '/finances')
      .then(function (response) {
        console.log(response);
        $scope.financeList = response.data.data;
        console.log($scope.financeList);
      });
  };
  $scope.dailyDart();

  // Create the signup modal that we will use later
  $ionicModal.fromTemplateUrl('templates/crawl/modals/stock-price.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
  });


  var limit = 15;
  // Open the stock price modal
  $scope.openStockPrice = function () {
    $scope.modal.show();
    // 회사정보 offset, limit으로 더 불러오기
    var loading = false;
    $scope.end = false;
    $scope.loadMore = function() {

      if (loading || $scope.end) return;
      loading = true;

      var params = {
        date: $filter('date')($scope.date, 'yyyy-MM-dd'),
        limit: limit,
        offset: $scope.stockList.length
      };
      $http.get(host + '/stocks', {params: params})
        .then(function (response) {
          loading = false;
          $scope.$broadcast('scroll.infiniteScrollComplete');

          $scope.stockList = $scope.stockList.concat(response.data.data);
          if (response.data.data.length < limit) {
            $scope.end = true;
          }
        }, function() {
          loading = false;
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.end = true;
        });

    };
    $scope.loadMore();
  };

  // Triggered in the stock price modal to close it
  $scope.closeStockPrice = function () {
    $scope.modal.hide();
  };

  // Create the finance modal that we will use later
  $ionicModal.fromTemplateUrl('templates/crawl/modals/finance.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.financeModal = modal;
  });


  // Open the stock price modal
  $scope.openFinance = function () {
    $scope.financeModal.show();
    // 재무정보 offset, limit으로 더 불러오기
    var loading = false;
    $scope.end = false;
    $scope.loadMore = function() {

      if (loading || $scope.end) return;
      loading = true;

      var params = {
        date: $filter('date')($scope.date, 'yyyy-MM-dd'),
        limit: limit,
        offset: $scope.financeList.length
      };
      $http.get(host + '/finances', {params: params})
        .then(function (response) {
          loading = false;
          $scope.$broadcast('scroll.infiniteScrollComplete');

          $scope.financeList = $scope.financeList.concat(response.data.data);
          if (response.data.data.length < limit) {
            $scope.end = true;
          }
        }, function() {
          loading = false;
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.end = true;
        });

    };
    $scope.loadMore();

  };

  // Triggered in the finance modal to close it
  $scope.closeFinance = function () {
    $scope.financeModal.hide();
  };


}]);
