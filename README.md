- [Get Started](#sec-1)

Cybos is a Korean stock trading platform. I developed an algorithm trading platform that uses Cybos and gathers relevant data from internet.
This repository is for the client side of the application which uses the Ionic framework (that uses Angular.js).

Basically, this client is to replace the GUI application of any stock trading platform and function as a dashboard.

# Get Started<a id="sec-1"></a>

```bash
npm install -g cordova ionic
npm install
ionic serve
```
